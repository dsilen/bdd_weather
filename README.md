

    mkdir bddsrvr
    cd bddsrvr
    dotnet new webapi
    dotnet sln add bddsrvr.csproj
    dotnet new gitignore
    echo .idea/ >> .gitignore
    git init
    git add -A
    git commit -m "Initial commit"

using bddsrvr.Code;
using Microsoft.AspNetCore.Mvc;

namespace bddsrvr.Controllers;

[ApiController]
//[Route("[controller]")]
//[RoutePrefix("api/example")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    [Route("weatherForecast")]
    public IEnumerable<WeatherForecast> Get()
    {
        int GetTemp() => GlobalState.RequestedWeather == WeatherType.Nice ? 25 : Random.Shared.Next(-20, 55);
        string GetSummary() => GlobalState.RequestedWeather == WeatherType.Nice ? "Nice" : Summaries[Random.Shared.Next(Summaries.Length)];
        
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            TemperatureC = GetTemp(),
            Summary = GetSummary()
        })
        .ToArray();
    }
    
    public class SetRequestedWeatherRequest
    {
        public string WeatherType { get; set; }
    }
    
    [HttpPost]
    [Route("setRequestedWeather")]
    public IActionResult SetRequestedWeather([FromBody] SetRequestedWeatherRequest requestedWeather)
    {
        try
        {
            GlobalState.RequestedWeather = Enum.Parse<WeatherType>(requestedWeather.WeatherType);
            return new OkResult();
        }
        catch (Exception ex)
        {
            _logger.LogWarning($"Exception caught: {ex}");
            return new BadRequestResult();
        }
    }
}
namespace bddsrvr.Code;

public enum WeatherType {
    Random,
    Freezing,
    Bracing,
    Chilly,
    Cool,
    Mild,
    Warm,
    Balmy,
    Hot,
    Sweltering,
    Scorching,
    Nice,
}